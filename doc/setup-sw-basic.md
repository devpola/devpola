[<< Previous Step: Hardware Assembly](/doc/setup-hw.md)

# Basic Raspberry Pi Setup

Download the [latest raspian image][0] (no desktop needed) and write it to the SD card.

Next, we need to make some general configurations using the [Raspberry Pi configuration tool][1].

```bash
sudo raspi-config
```
Mandatory Settings:
* ```Advanced Options > Expand Filesystem```: this ensures that all of the SD card storage is available to the OS
* ```Interfacing Options > Camera```: enable the Raspberry Pi to work with the camera
* Disable login shell and enable serial port hardware via ```Interfacing Options > Serial```

Optional Settings:
* ```Change User Password```
* ```Localisation Options > Change Keyboard Layout```
* Set hostname via ```Network Optons > Hostname```
* Enable SSH via ```Interfacing Options > SSH```
* Disable overscan via ```Advanced Options > Overscan```

When you're done reboot the system and ensure that the raspberry is connected to the Internet for the following steps.

## Update and Upgrade the System

```bash
sudo apt-get update
sudo apt-get upgrade
```

## Test Camera

Type the following command in the shell. If you see a live preview on your screen the camera works properly.
```bash
# show a 1 second live preview and store image as /tmp/test.jpg
raspistill -f -t 3000 -o /tmp/test.jpg
```

Interested in more? Visit the the [Raspberry Pi website](https://www.raspberrypi.org/documentation/usage/camera/raspicam/README.md).

## Install Printer Support

Install some basic packages:
```bash
sudo apt-get install git cups wiringpi build-essential libcups2-dev libcupsimage2-dev
```

Install raster filter for CUPS:
```bash
cd
git clone https://github.com/adafruit/zj-58
cd zj-58
make
sudo ./install
```

Add printer to the CUPS system and set it as default:
```bash
# <BAUDRATE> needs to be replaced by actual printer's baud rate
sudo lpadmin -p ZJ-58 -E -v serial:/dev/serial0?baud=<BAUDRATE> -m zjiang/ZJ-58.ppd
sudo lpoptions -d ZJ-58
```

Optionally test the printer:
1. ensure that the printer is properly connected (see Hardware section)
2. send a sample text to the printer
```
stty -F /dev/serial0 <BAUDRATE>
echo -e "This is a test.\\n\\n\\n" > /dev/serial0
```

Interested in more? There's a nice [Adafruit tutorial](https://learn.adafruit.com/networked-thermal-printer-using-cups-and-raspberry-pi?view=all).

## Display

In case you use an HDMI display you do not need to install anything further because HDMI connection works out of the box.

GPIO displays, however, need some further installation steps which are depending on the display's model. I'll cover one model which I've used so far.

### Setup Adafruit PiTFT Plus 3.5"

In case you are also using an **Adafruit PiTFT Plus 3.5"** please follow the steps described [here](/doc/setup-sw-display-adafruit-pitft-3-5.md).


[>> Next Step: Deploying /dev/pola](/doc/setup-sw-devpola.md)


[0]: https://www.raspberrypi.org/downloads/raspbian/
[1]: https://www.raspberrypi.org/documentation/configuration/raspi-config.md


