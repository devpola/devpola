## Parts

* **Raspberry Pi** + (micro) SD card for operating system
  * So far I've used one of my old "Raspberry Pi 1 Model B revision 1.2" 
  * ... and a "Raspberry Pi 3 Model B"
* Raspberry Pi **Camera** (inclusive Flex Cable)
  * I've used the "Raspberry Pi Noir Camera"
* Raspberry Pi **Display** (either GPIO or HDMI)
  * So far I've used an HDMI display => [Kuman 3.5" HDMI MPI3508][0] - however this may not be the best choice as there are no holes for screwing it onto the case. You also need an HDMI ribbon cable (in case you go for an HDMI display) [Amazon Example][1], [Adafruit Examples][2].
  * ... and a GPIO display => [Adafruit PiTFT Plus 3.5"][3].
* **Thermal Printer** (TTL, serial) + Thermal Paper Rolls
  * I've used [Adafruit's Mini Thermal Receipt Printer][4] 
  * ... and 58mm (15m) thermal paper rolls
* **Momentary Push Buttons**
  * eg 16 mm: [Adafruit Examples][5]
* **On/Off Switch**
* **LIPO battery** 7,4V 
  * So far I've worked with [SLS Quantum 2200mAh 2S1P 7,4V 30C/60C][6] and [SLS Quantum 2200mAh 2S1P 7,4V 40C/80C][7] 
* **DC DC Step-down**
  * eg [UBEC 5V 3A][8] 
  * ... or [adjustable ones][9]
* **Case**
  * You can use a cardboard box 
  * ... or eg Thomas Kremser's [wooden laser cut one][10].
* a lot of cables


[>> Next Step: Hardware Assembly](/doc/setup-hw.md)


[0]: http://www.kumantech.com/kuman-35quot-inch-19201080-full-hd-tft-lcd-display-monitor-for-all-raspberry-pi-system-sc6a_p0394.html
[1]: https://www.amazon.de/dp/B01E6UKVKS/ref=pe_3044161_185740101_TE_item?language=de_DE
[2]: https://www.adafruit.com/?q=hdmi%20cable
[3]: https://www.adafruit.com/product/2441
[4]: https://www.adafruit.com/product/597
[5]: https://www.adafruit.com/?q=16%20mm%20momentary%20push%20button&p=1
[6]: https://www.stefansliposhop.de/Akkus/SLS-QUANTUM/SLS-QUANTUM-30C/SLS-Quantum-2200mAh-2S1P-7-4V-30C-60C::1658.html
[7]: https://www.stefansliposhop.de/Akkus/SLS-QUANTUM/SLS-QUANTUM-40C/SLS-Quantum-2200mAh-2S1P-7-4V-40C-80C::1707.html
[8]: https://www.adafruit.com/product/1385
[9]: https://www.amazon.de/dp/B01GJ0SC2C/ref=pe_3044161_185740101_TE_item_image
[10]: https://cdn.hackaday.io/files/18464789963776/Case3.0.eps
